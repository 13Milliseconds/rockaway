<?php

require "vendor/autoload.php";
use CrudKit\CrudKitApp;
use CrudKit\Pages\BasicLoginPage;
use CrudKit\Pages\MySQLTablePage;

// Create a new CrudKitApp object
$app = new CrudKitApp ();
$app->setStaticRoot ("static/crudkit/");
$app->setAppName ("Admin Panel");

// 
// HANDLE LOGIN
// 
$login = new BasicLoginPage ();
$login->setWelcomeMessage ("Use credentials admin/demo or user/demo");
if ($login->userTriedLogin ()) {
    $username = $login->getUserName ();
    $password = $login->getPassword ();

    // TODO: you should use your own authentication scheme here
    if ($username === 'riva' && $password === 'password') {
        $login->success ();
    }
    else {
        $login->fail ("Please check your password");
    }
}
$app->useLogin ($login);
// 
// END HANDLING LOGIN.
// 

$mysql_page = new MySQLTablePage ("business_page", "onethrmi_rockawa", "+<N5+5}Wf%mq9!%r", "onethrmi_rockaway", "localhost", "8889");
$mysql_page
    ->setName ("Businesses")
    ->setTableName ("Businesses")
    ->setRowsPerPage (25)
    ->setPrimaryColumn("business_id")
    ->addColumn("name", "Name", [
            'validation' => [
                'required' => true,
            ]])
    ->addColumn("first", "First Name", [
            'validation' => [
                'required' => false,
            ]])
    ->addColumn("last", "Last Name")
    ->addColumn("address", "Address")
    ->addColumn("industry", "Industry")
    //->addColumn("left", "Left coordinate")
    ->addColumn("left_perc", "Bttom coordinate")
    ->addColumn("bottom_perc", "Bottom coordinate")
    ->addColumn("description", "Description")
    ->addColumn("share", "Share")
    ->addColumn("email", "Email")
    ->addColumn("website", "Website")
    ->addColumn("phone", "Phone")
    ->addColumn("instagram", "Instagram")
    ->addColumn("facebook", "Facebook")
    ->setSummaryColumns(array("name", "first", "last", "address"));
$app->addPage ($mysql_page);


// Render the app. This will display the HTML
$app->render ();
