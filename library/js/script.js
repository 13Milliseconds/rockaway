/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define */
/*globals $:false */

$(document).ready(function () {
    'use strict';
    
    var $query = 'UPDATE `Businesses`\n SET `left` = CASE `business_id`\n',
        $queryleft = '',
        $querybottom = '',
        $width = $(window).innerWidth(),
        $wrapWidth,
        $height,
        $newheight,
        $newwidth,
        $scroll,
        $grid = $('#grid-wrap'),
        id,
        $selectTop,
        i,
        value;
    
    $('#form_submit').click(function () {
        $('.line').each(function () {
            var id = $(this).attr('id'),
                left = $('.left', this).val(),
                bottom = $('.bottom', this).val();
            
            $queryleft += 'WHEN ' + id + ' THEN ' + left + '\n';
            $querybottom += 'WHEN ' + id + ' THEN ' + bottom + '\n';
        });
        
        $query += $queryleft;
        $query += 'END,\n`bottom` = CASE `business_id`\n';
        $query += $querybottom;
        $query += 'END';
        
        console.log($query);
        
        $.ajax({
            url: 'coordinates.php',
            type: 'POST',
            data: { query: $query },
            success: function (result) {
                console.log('Sent!');
            }
        });
        
        location.reload();
        
        return false;
       
    });
    
    //Reveal the info
    function Reveal() {
        $('#clickstop').fadeIn('fast');
        $('.active').removeClass('active');
        $("html, body").animate({ scrollTop: "0px" });
        var linkid = $(this).attr('id').replace('circle-', '').replace('index-', '');
        console.log(linkid);
        $('#info-' + linkid).addClass('active').show();
    }
    
    //Adding a grid for easy targeting
    if ($('#coordinates').length) {
        for (i = 0; i < 100; i++) {
            $('#map').append('<div class="grid"></div>');
        }
    }
    
    $('#map').prepend('<div id="clickstop"></div>');
    

    $('#map .circle').hover(function () {
        id = $(this).attr('id').replace('circle-', '');
        $('#info-' + id).show();
    }, function () {
        if (!$('#info-' + id).hasClass('active')) {
            $('#info-' + id).hide();
        }
    });
    $('#map .circle').on('click', Reveal);
    
    $('#clickstop').click(function () {
        $('.active').fadeOut('fast');
        $('.active').removeClass('active');
        $(this).fadeOut();
    });
    
    //Mobile header
    if ($width <= 1030) {
        var getSize = window.setTimeout(function () {
            $selectTop =  $('#select').offset().top;
        }, 10);
    
    
        $(window).scroll(function () {
            $scroll = $(window).scrollTop();
            if ($scroll > $selectTop) {
                $('#select').addClass('fixed');
            } else {
                $('#select').removeClass('fixed');
            }
        });
        
    }
    
    
    
    //Copying all info boxes and adding to the index
    $('.info').each(function () {
        $(this).clone().appendTo('#grid-wrap');
    });
    $('#grid-wrap .info').css('left', '').css('bottom', '');
    $('#grid-wrap .info').each(function () {
        var id = $(this).attr('id').replace('info-', 'index-');
        $(this).attr('id', '');
        $('h2', this).attr('id', id);
    });
    $('#grid-wrap .info h2').on('click', Reveal);
    
    
    
    //Social Share
    $('a.facebook').click(function () {
        FB.ui({
            method: 'share',
            href: 'http://map.ladiesofbusinessrockaway.org/'
        }, function (response) {});
    });
    
    
    
    //Select the category
    $('#catselect').change(function () {
        value = $('#catselect').val();
        if (value !== 'null') {
            // filter by value
            $('#grid-wrap .info').hide();
            $('#grid-wrap .' + value).show();
        } else {
            // show all items
            $('#grid-wrap .info').show();
        }
    });
    
    
    
    //resizing the map
    function resize() {
        
        $wrapWidth = $('#wrap').innerWidth();
        $height = $(window).height();
        $newheight = $wrapWidth * 77.2 / 100;
        $newwidth = $height * 100 / 77.2;
        
        if ($newheight > $height) {
            $('#map').css('height', $height);
            $('#map').css('width', $newwidth);
        } else {
            $('#map').css('width', $wrapWidth);
            $('#map').css('height', $newheight);
        }
    }
    
    resize();
    $(window).resize(resize);
    
    
});