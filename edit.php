<!DOCTYPE HTML>
<html>
<head>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="library/css/style.css">
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
    <script src="library/js/script.js"></script>
    
</head>
<body class="edit">
    
    <?php
      DEFINE('DB_USERNAME', 'onethrmi_rockawa');
      DEFINE('DB_PASSWORD', '+<N5+5}Wf%mq9!%r');
      DEFINE('DB_HOST', 'localhost');
      DEFINE('DB_DATABASE', 'onethrmi_rockaway');

      $mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

      if (mysqli_connect_error()) {
        die('Connect Error ('.mysqli_connect_errno().') '.mysqli_connect_error());
      }
        $sql="SELECT * FROM `Businesses`";
        
    if ($result = $mysqli->query($sql)) {
    while ($row = $result->fetch_assoc()) {
        $result->result_array[] = $row;
    }
    }; ?>
    
    <?php 
        // Function to sort array alphabetically
    function array_sort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();
        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }
            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }
        return $new_array;
    }
        //Sort the array
    $sorted = array_sort($result->result_array, 'name'); 
        ?>
    
    
    <div id="wrap" class="col-2of3">
    <div id="map" style="background-image: url('library/images/map-021417.png')">
    
    <?php $i = 1;
        
        //Display the dots on the map
     foreach($sorted as $row):   
        $ID = $row[business_id];
        $name = $row[name];
        $address = $row[address];
        $address = substr($address, 0, strpos($address, '.,'));
        $email = $row[email];
        $phone = $row[phone];
        $facebook = $row[facebook];
        $instagram = str_replace("@","",$row[instagram]);
        $share = $row[share]; 
        $left = $row[left_perc]; 
        $bottom = $row[bottom_perc];
        $description = $row[description];
        $industry = $row[industry];
        $industry = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $industry));
        
        $topthreshold = 60;
        if ($bottom < $topthreshold) {
            $Yvalue = 'bottom: '.$bottom.'%;';
        } else {
            $Yvalue = 'top: '.(100 - $bottom).'%;';
        }
    ?>
        
    <div class="info <?php echo $industry; ?> <?php if ($left > 50) { echo 'right'; } ?> <?php if ($bottom > $topthreshold) { echo 'top'; }?>" id="info-<?php echo $i ?>" style="left: <?php echo $left; ?>%; <?php echo $Yvalue; ?>">
        <div class="arrow-top"></div>
        <div class="info-wrap">
            <h2><?php echo $name; ?></h2>
            <p><?php echo $description; ?>.</p>
            <?php if ($address) {?>
                <i class="fa fa-globe" aria-hidden="true"></i> <?php echo $address ?><br>
            <?php } ?>
            <?php if ($facebook) {echo '<a href="' . $facebook . '" target=\"_blank\"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a><br>'; } ?>
            <?php if ($instagram) {echo '<a href="http://instagram.com/' . $instagram . '" target=\"_blank\"><i class="fa fa-instagram" aria-hidden="true"></i> ' . $instagram . '</a><br>'; } ?>
        </div>
        <div class="arrow"></div>
    </div>
        
    <div id="circle-<?php echo $i ?>" class="circle <?php echo $industry; ?>" style="left: <?php echo $left; ?>%; <?php echo $Yvalue; ?>">
        <?php echo $i; ?>
    </div>
    
<?php   $i++; 
        endforeach; ?>
        </div>
        
        </div>
    
    <div class="col-1of3">
    <div id="coordinates">
    <?php
        $i = 1;
    foreach($sorted as $row):
        $ID = $row[business_id];
        $name = $row[name];
        $left = $row[left_perc]; 
        $bottom = $row[bottom_perc]; 
        $address = $row[address];

    ?>
        <div class="line" id="<?php echo $ID ?>">
            <h3><?php echo $i ?> - <?php echo $name ?></h3>
            <i class="fa fa-globe" aria-hidden="true"></i> <?php echo $address ?><br>
            <input class="left" value="<?php echo $left; ?>">
            <input class="bottom" value="<?php echo $bottom; ?>">
        </div>
    
        <?php $i++;
        endforeach; ?>
        </div>
    </div>
        <input type="submit" value="Submit" id="form_submit">
    <?php $mysqli->close(); ?>
        
</body>
</html>