<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


require_once __DIR__.'/../../../vendor/autoload.php';
require_once __DIR__.'/../../../src/app.php';

use Symfony\Component\Validator\Constraints as Assert;

$app->match('/Businesses/list', function (Symfony\Component\HttpFoundation\Request $request) use ($app) {  
    $start = 0;
    $vars = $request->request->all();
    $qsStart = (int)$vars["start"];
    $search = $vars["search"];
    $order = $vars["order"];
    $columns = $vars["columns"];
    $qsLength = (int)$vars["length"];    
    
    if($qsStart) {
        $start = $qsStart;
    }    
	
    $index = $start;   
    $rowsPerPage = $qsLength;
       
    $rows = array();
    
    $searchValue = $search['value'];
    $orderValue = $order[0];
    
    $orderClause = "";
    if($orderValue) {
        $orderClause = " ORDER BY ". $columns[(int)$orderValue['column']]['data'] . " " . $orderValue['dir'];
    }
    
    $table_columns = array(
		'business_id', 
		'left', 
		'bottom', 
		'name', 
		'first', 
		'last', 
		'description', 
		'address', 
		'share', 
		'website', 
		'phone', 
		'email', 
		'instagram', 
		'facebook', 
		'industry', 

    );
    
    $table_columns_type = array(
		'int(11)', 
		'decimal(11,1)', 
		'decimal(11,1)', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 
		'text', 

    );    
    
    $whereClause = "";
    
    $i = 0;
    foreach($table_columns as $col){
        
        if ($i == 0) {
           $whereClause = " WHERE";
        }
        
        if ($i > 0) {
            $whereClause =  $whereClause . " OR"; 
        }
        
        $whereClause =  $whereClause . " " . $col . " LIKE '%". $searchValue ."%'";
        
        $i = $i + 1;
    }
    
    $recordsTotal = $app['db']->executeQuery("SELECT * FROM `Businesses`" . $whereClause . $orderClause)->rowCount();
    
    $find_sql = "SELECT * FROM `Businesses`". $whereClause . $orderClause . " LIMIT ". $index . "," . $rowsPerPage;
    $rows_sql = $app['db']->fetchAll($find_sql, array());

    foreach($rows_sql as $row_key => $row_sql){
        for($i = 0; $i < count($table_columns); $i++){

		if( $table_columns_type[$i] != "blob") {
				$rows[$row_key][$table_columns[$i]] = $row_sql[$table_columns[$i]];
		} else {				if( !$row_sql[$table_columns[$i]] ) {
						$rows[$row_key][$table_columns[$i]] = "0 Kb.";
				} else {
						$rows[$row_key][$table_columns[$i]] = " <a target='__blank' href='menu/download?id=" . $row_sql[$table_columns[0]];
						$rows[$row_key][$table_columns[$i]] .= "&fldname=" . $table_columns[$i];
						$rows[$row_key][$table_columns[$i]] .= "&idfld=" . $table_columns[0];
						$rows[$row_key][$table_columns[$i]] .= "'>";
						$rows[$row_key][$table_columns[$i]] .= number_format(strlen($row_sql[$table_columns[$i]]) / 1024, 2) . " Kb.";
						$rows[$row_key][$table_columns[$i]] .= "</a>";
				}
		}

        }
    }    
    
    $queryData = new queryData();
    $queryData->start = $start;
    $queryData->recordsTotal = $recordsTotal;
    $queryData->recordsFiltered = $recordsTotal;
    $queryData->data = $rows;
    
    return new Symfony\Component\HttpFoundation\Response(json_encode($queryData), 200);
});




/* Download blob img */
$app->match('/Businesses/download', function (Symfony\Component\HttpFoundation\Request $request) use ($app) { 
    
    // menu
    $rowid = $request->get('id');
    $idfldname = $request->get('idfld');
    $fieldname = $request->get('fldname');
    
    if( !$rowid || !$fieldname ) die("Invalid data");
    
    $find_sql = "SELECT " . $fieldname . " FROM " . Businesses . " WHERE ".$idfldname." = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($rowid));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('menu_list'));
    }

    header('Content-Description: File Transfer');
    header('Content-Type: image/jpeg');
    header("Content-length: ".strlen( $row_sql[$fieldname] ));
    header('Expires: 0');
    header('Cache-Control: public');
    header('Pragma: public');
    ob_clean();    
    echo $row_sql[$fieldname];
    exit();
   
    
});



$app->match('/Businesses', function () use ($app) {
    
	$table_columns = array(
		'business_id', 
		'left', 
		'bottom', 
		'name', 
		'first', 
		'last', 
		'description', 
		'address', 
		'share', 
		'website', 
		'phone', 
		'email', 
		'instagram', 
		'facebook', 
		'industry', 

    );

    $primary_key = "business_id";	

    return $app['twig']->render('Businesses/list.html.twig', array(
    	"table_columns" => $table_columns,
        "primary_key" => $primary_key
    ));
        
})
->bind('Businesses_list');



$app->match('/Businesses/create', function () use ($app) {
    
    $initial_data = array(
		'left' => '', 
		'bottom' => '', 
		'name' => '', 
		'first' => '', 
		'last' => '', 
		'description' => '', 
		'address' => '', 
		'share' => '', 
		'website' => '', 
		'phone' => '', 
		'email' => '', 
		'instagram' => '', 
		'facebook' => '', 
		'industry' => '', 

    );

    $form = $app['form.factory']->createBuilder('form', $initial_data);



	$form = $form->add('left', 'text', array('required' => true));
	$form = $form->add('bottom', 'text', array('required' => true));
	$form = $form->add('name', 'textarea', array('required' => true));
	$form = $form->add('first', 'textarea', array('required' => true));
	$form = $form->add('last', 'textarea', array('required' => true));
	$form = $form->add('description', 'textarea', array('required' => true));
	$form = $form->add('address', 'textarea', array('required' => true));
	$form = $form->add('share', 'textarea', array('required' => true));
	$form = $form->add('website', 'textarea', array('required' => true));
	$form = $form->add('phone', 'textarea', array('required' => true));
	$form = $form->add('email', 'textarea', array('required' => true));
	$form = $form->add('instagram', 'textarea', array('required' => true));
	$form = $form->add('facebook', 'textarea', array('required' => true));
	$form = $form->add('industry', 'textarea', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        if ($form->isValid()) {
            $data = $form->getData();

            $update_query = "INSERT INTO `Businesses` (`left`, `bottom`, `name`, `first`, `last`, `description`, `address`, `share`, `website`, `phone`, `email`, `instagram`, `facebook`, `industry`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $app['db']->executeUpdate($update_query, array($data['left'], $data['bottom'], $data['name'], $data['first'], $data['last'], $data['description'], $data['address'], $data['share'], $data['website'], $data['phone'], $data['email'], $data['instagram'], $data['facebook'], $data['industry']));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'Businesses created!',
                )
            );
            return $app->redirect($app['url_generator']->generate('Businesses_list'));

        }
    }

    return $app['twig']->render('Businesses/create.html.twig', array(
        "form" => $form->createView()
    ));
        
})
->bind('Businesses_create');



$app->match('/Businesses/edit/{id}', function ($id) use ($app) {

    $find_sql = "SELECT * FROM `Businesses` WHERE `business_id` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('Businesses_list'));
    }

    
    $initial_data = array(
		'left' => $row_sql['left'], 
		'bottom' => $row_sql['bottom'], 
		'name' => $row_sql['name'], 
		'first' => $row_sql['first'], 
		'last' => $row_sql['last'], 
		'description' => $row_sql['description'], 
		'address' => $row_sql['address'], 
		'share' => $row_sql['share'], 
		'website' => $row_sql['website'], 
		'phone' => $row_sql['phone'], 
		'email' => $row_sql['email'], 
		'instagram' => $row_sql['instagram'], 
		'facebook' => $row_sql['facebook'], 
		'industry' => $row_sql['industry'], 

    );


    $form = $app['form.factory']->createBuilder('form', $initial_data);


	$form = $form->add('left', 'text', array('required' => true));
	$form = $form->add('bottom', 'text', array('required' => true));
	$form = $form->add('name', 'textarea', array('required' => true));
	$form = $form->add('first', 'textarea', array('required' => true));
	$form = $form->add('last', 'textarea', array('required' => true));
	$form = $form->add('description', 'textarea', array('required' => true));
	$form = $form->add('address', 'textarea', array('required' => true));
	$form = $form->add('share', 'textarea', array('required' => true));
	$form = $form->add('website', 'textarea', array('required' => true));
	$form = $form->add('phone', 'textarea', array('required' => true));
	$form = $form->add('email', 'textarea', array('required' => true));
	$form = $form->add('instagram', 'textarea', array('required' => true));
	$form = $form->add('facebook', 'textarea', array('required' => true));
	$form = $form->add('industry', 'textarea', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        if ($form->isValid()) {
            $data = $form->getData();

            $update_query = "UPDATE `Businesses` SET `left` = ?, `bottom` = ?, `name` = ?, `first` = ?, `last` = ?, `description` = ?, `address` = ?, `share` = ?, `website` = ?, `phone` = ?, `email` = ?, `instagram` = ?, `facebook` = ?, `industry` = ? WHERE `business_id` = ?";
            $app['db']->executeUpdate($update_query, array($data['left'], $data['bottom'], $data['name'], $data['first'], $data['last'], $data['description'], $data['address'], $data['share'], $data['website'], $data['phone'], $data['email'], $data['instagram'], $data['facebook'], $data['industry'], $id));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'Businesses edited!',
                )
            );
            return $app->redirect($app['url_generator']->generate('Businesses_edit', array("id" => $id)));

        }
    }

    return $app['twig']->render('Businesses/edit.html.twig', array(
        "form" => $form->createView(),
        "id" => $id
    ));
        
})
->bind('Businesses_edit');



$app->match('/Businesses/delete/{id}', function ($id) use ($app) {

    $find_sql = "SELECT * FROM `Businesses` WHERE `business_id` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if($row_sql){
        $delete_query = "DELETE FROM `Businesses` WHERE `business_id` = ?";
        $app['db']->executeUpdate($delete_query, array($id));

        $app['session']->getFlashBag()->add(
            'success',
            array(
                'message' => 'Businesses deleted!',
            )
        );
    }
    else{
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );  
    }

    return $app->redirect($app['url_generator']->generate('Businesses_list'));

})
->bind('Businesses_delete');






