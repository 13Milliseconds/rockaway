<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Rockaway Business</title>
    
<?php // mobile meta (hooray!) ?>
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    
<?php // Opengraph ?>
<meta property="og:url"                content="http://map.ladiesofbusinessrockaway.org" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="Ladies of Business Rockaway Beach Interactive Map" />
<meta property="og:description"        content="Discover and support Rockaway's women-owned businesses offering everything from food and drink to health and wellness to fashion and finance." />
<meta property="og:image"              content="http://map.ladiesofbusinessrockaway.org/library/images/facebook.jpg" />

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="library/css/style.css?v=1.0.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script src="library/js/script.js"></script>
<script src="library/js/isotope.pkgd.min.js"></script>
    
</head>
<body>
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '268190790294322',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    
    <div id="main" class="clearfix">
    <?php
  DEFINE('DB_USERNAME', 'onethrmi_rockawa');
  DEFINE('DB_PASSWORD', '+<N5+5}Wf%mq9!%r');
  DEFINE('DB_HOST', 'localhost');
  DEFINE('DB_DATABASE', 'onethrmi_rockaway');

  $mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

  if (mysqli_connect_error()) {
    die('Connect Error ('.mysqli_connect_errno().') '.mysqli_connect_error());
  }
    $sql="SELECT * FROM `Businesses`";
        
    if ($result = $mysqli->query($sql)) {
    while ($row = $result->fetch_assoc()) {
        $result->result_array[] = $row;
    }
    }; ?>
    
    
    <?php 
        // Function to sort array alphabetically
    function array_sort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();
        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }
            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }
        return $new_array;
    }
        //Sort the array
    $sorted = array_sort($result->result_array, 'name');
        ?>
    
    
    <div id="wrap">
    <div id="map">
    
    <?php $i = 1;
        
        //Display the dots on the map
        $industries = [];
        
     foreach($sorted as $row):   
        $ID = $row[business_id];
        $name = $row[name];
        $share = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $row[share]));
        $address = $row[address];
        $email = $row[email];
        $phone = $row[phone];
        $facebook = $row[facebook];
        $instagram = str_replace("@","",$row[instagram]);
        $left = $row[left_perc]; 
        $bottom = $row[bottom_perc];
        $phone = $row[phone];
        $description = $row[description];
        $website = $row[website];
        $industry = $row[industry];
        $industrytag = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $industry));
        if (!in_array ($industry , $industries)) {
            $industries[$industry] = $industrytag ;
        }
        
        $topthreshold = 50;
        if ($bottom < $topthreshold) {
            $Yvalue = 'bottom: '.$bottom.'%;';
        } else {
            $Yvalue = 'top: '.(100 - $bottom).'%;';
        }
    ?>
        
    <div class="info <?php echo $industrytag; ?> <?php if ($left > 50) { echo 'right'; } ?> <?php if ($bottom > $topthreshold) { echo 'top'; }?>" id="info-<?php echo $i ?>" style="left: <?php echo $left; ?>%; <?php echo $Yvalue; ?>">
        <div class="arrow-top"></div>
        <div class="info-wrap">
            <h2><?php echo $name; ?></h2>
            <p><?php echo $description; ?>.</p>
            <?php if ($share == "yes") {
                    if ($address) {?>
                    <i class="fa fa-globe" aria-hidden="true"></i> 
                    <a href="http://maps.google.com/?q=<?php echo $address; ?>" target="_blank">
                    <?php echo $address ?>
                    </a><br>
            <?php }} ?>
            <?php if ($phone) { ?>
            <i class="fa fa-phone" aria-hidden="true"></i>
            <?php echo $phone; ?><br>
            <?php } ?>
            <?php if ($email) { ?>
            <i class="fa fa-envelope" aria-hidden="true"></i>
            <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br>
            <?php } ?>
            <?php if ($website) { ?>
            <i class="fa fa-external-link-square" aria-hidden="true"></i>
            <a href="<?php echo $website; ?>" target="_blank">Website</a><br>
            <?php } ?>
            <?php if ($facebook) { ?>
            <i class="fa fa-facebook" aria-hidden="true"></i>
            <a href="<?php echo $facebook; ?>" target="_blank">Facebook</a><br>
            <?php } ?>
            <?php if ($instagram) { ?>
            <i class="fa fa-instagram" aria-hidden="true"></i>
            <a href="http://instagram.com/<?php echo $instagram; ?>" target="_blank"><?php echo $instagram; ?></a><br>
            <?php } ?>
        </div>
        <div class="arrow"></div>
    </div>
        
    <div id="circle-<?php echo $i ?>" class="circle <?php if ($share == "no") {echo "hiddenaddress"; } ?> <?php echo $industrytag; ?>" style="left: <?php echo $left; ?>%; <?php echo $Yvalue; ?>">
    </div>
    
<?php   $i++; 
        endforeach; ?>
        </div>
        </div>
        <div id="legend">
            <h2>Legend</h2>
        <ul>
            <?php ksort($industries);
            foreach ($industries as $cat => $catshort): ?>
            <li class="category">
                <div class="circle <?php echo $catshort; ?>"></div>
                <?php echo $cat; ?>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        
    <div id="social">
            <a href="https://www.ladiesofbusinessrockaway.org/" target="_blank"><div class="social website"><i class="fa fa-link" aria-hidden="true"></i></div></a>
            <a class="social facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a class="social twitter" href="https://twitter.com/share?url=http%3A%2F%2Fmap.ladiesofbusinessrockaway.org&related=twitterapi%2Ctwitter&hashtags=loveyourlocal%2Cshopsmall&text=Check%20out%20this%20fun%20map%20of%20women-owned%20businesses%20in%20NYC's%20hip%20surf%20town,%20Rockaway Beach!" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
        
    <div id="fullIndex">
        <div id="select">
            <select id="catselect">
                <option value="null">Categories</option>
                <?php foreach ($industries as $cat => $catshort): ?>
                    <option value="<?php echo $catshort; ?>"><?php echo $cat; ?></option>
                <?php endforeach; ?>    
            </select>
        </div>
    <h1>Business Listings</h1>
        <div id="grid-wrap">
            <?php // Loaded via Javascript ?>
        </div>
    </div>
        
    <?php $mysqli->close(); ?>
        </div>
</body>
</html>